﻿using Urs.Core.Configuration;

namespace Urs.Data.Domain.Configuration
{
    /// <summary>
    /// 模板设置 settings
    /// </summary>
    public class TemplateSettings : ISettings
    {
        public int Picture1Id { get; set; }
        public string Text1 { get; set; }
        public string SubText1 { get; set; }
        public string Time1 { get; set; }
        public string Ext1 { get; set; }
        public string AltText1 { get; set; }
        public bool Default1 { get; set; }

        public int Picture2Id { get; set; }
        public string Text2 { get; set; }
        public string SubText2 { get; set; }
        public string Time2 { get; set; }
        public string Ext2 { get; set; }
        public string AltText2 { get; set; }
        public bool Default2 { get; set; }

        public int Picture3Id { get; set; }
        public string Text3 { get; set; }
        public string SubText3 { get; set; }
        public string Time3 { get; set; }
        public string Ext3 { get; set; }
        public string AltText3 { get; set; }
        public bool Default3 { get; set; }

        public int Default { get; set; }
        public bool Enabled { get; set; }

        public int TianQiId1 { get; set; }
        public int TianQiId2 { get; set; }
        public int TianQiId3 { get; set; }
        public int TianQiId4 { get; set; }
        public int TianQiId5 { get; set; }
        public int TianQiId6 { get; set; }
        public int TianQiId7 { get; set; }
    }
}