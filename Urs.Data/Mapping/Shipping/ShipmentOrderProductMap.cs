
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Urs.Data.Domain.Shipping;

namespace Urs.Data.Mapping.Shipping
{
    public partial class ShipmentOrderGoodsMap : UrsEntityTypeConfiguration<ShipmentOrderItem>
    {
        public override void Configure(EntityTypeBuilder<ShipmentOrderItem> builder)
        {
            builder.ToTable(nameof(ShipmentOrderItem));
            builder.HasKey(sopv => sopv.Id);

            builder.HasOne(sopv => sopv.Shipment)
                .WithMany(s => s.ShipmentorderItems)
                .HasForeignKey(sopv => sopv.ShipmentId);
            base.Configure(builder);
        }
    }
}