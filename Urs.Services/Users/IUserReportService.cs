using System;
using System.Collections.Generic;
using Urs.Data.Domain.Users;
using Urs.Data.Domain.Orders;
using Urs.Data.Domain.Payments;
using Urs.Data.Domain.Shipping;

namespace Urs.Services.Users
{
    public partial interface IUserReportService
    {
        IList<BestUserReportLine> GetBestUsersReport(DateTime? startTime,
            DateTime? endTime, OrderStatus? os, PaymentStatus? ps, ShippingStatus? ss, int orderBy);
        
        int GetRegisteredUsersReport(int days);
    }
}