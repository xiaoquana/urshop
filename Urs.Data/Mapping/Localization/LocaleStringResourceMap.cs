
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Urs.Data.Domain.Localization;

namespace Urs.Data.Mapping.Localization
{
    public partial class LocaleStringResourceMap : UrsEntityTypeConfiguration<LocaleStringResource>
    {
        public override void Configure(EntityTypeBuilder<LocaleStringResource> builder)
        {
            builder.ToTable(nameof(LocaleStringResource));

            builder.HasKey(lsr => lsr.Id);
            builder.Property(lsr => lsr.Name).IsRequired().HasMaxLength(200);
            builder.Property(lsr => lsr.Value).IsRequired();

            base.Configure(builder);

        }
    }
}