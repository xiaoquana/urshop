﻿using Urs.Framework;
using Urs.Framework.Mvc;

namespace Urs.Admin.Models.Localization
{
    public class LocalizationListModel 
    {
        [UrsDisplayName("Admin.Configuration.Resources.SearchResourceName")]
        public string SearchName { get; set; }
        [UrsDisplayName("Admin.Configuration.Resources.SearchResourceValue")]
        public string SearchValue { get; set; }
    }
}