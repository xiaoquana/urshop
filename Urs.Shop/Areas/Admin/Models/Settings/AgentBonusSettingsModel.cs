﻿using System.ComponentModel;
using Urs.Framework.Models;
using Urs.Framework.Mvc;

namespace Urs.Admin.Models.Settings
{
    public partial class AgentBonusSettingsModel : BaseModel, ISettingsModel
    {
        /// <summary>
        /// 分红一级佣金比率
        /// </summary>
        [DisplayName("一级佣金比例如：8%")]
        public decimal Rate { get; set; }
        /// <summary>
        /// 分红二级佣金比率
        /// </summary>
        [DisplayName("二级佣金比例如：5%")]
        public decimal ParentRate { get; set; }
        /// <summary>
        /// 分红三级佣金比率
        /// </summary>
        [DisplayName("三级佣金比例如：5%")]
        public decimal GrandParentRate { get; set; }
        /// <summary>
        /// 佣金计算方式
        /// </summary>
        [DisplayName("佣金计算方式")]
        public int Method { get; set; }
        /// <summary>
        /// 佣金兑换状态
        /// </summary>
        [DisplayName("佣金兑换状态")]
        public int BonusForHandout_Awarded { get; set; }
        /// <summary>
        /// 取消状态
        /// </summary>
        [DisplayName("取消状态")]
        public int BonusForHandout_Canceled { get; set; }

        /// <summary>
        /// 二级分销启用
        /// </summary>
        public bool ParentAgentEnabled { get; set; }
        /// <summary>
        /// 三级分销启用
        /// </summary>
        public bool GrandParentAgentEnabled { get; set; }
    }
}