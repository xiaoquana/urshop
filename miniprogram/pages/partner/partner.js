import api from '../../api/api'
import { formsubmit } from '../../api/conf'
Page({

  data: {
    region: ['江苏省', '徐州市', '全部'],
    customItem: '全部',
    array: ['别墅区', '城中村', '高端小区', '一般小区'],
    index: 0,
  },
  onLoad: function (options) {
    
  },
  onReady: function () {
    
  },
  bindRegionChange: function (e) {
    this.setData({
      region: e.detail.value
    })
  },
  bindPickerChange: function (e) {
    this.setData({
      index: e.detail.value
    })
  },
  formSubmit: function (e) {
    let phone = parseInt(e.detail.value.Phone)
    if (!(/^1[34578]\d{9}$/.test(phone))){
      wx.showToast({
        title: '手机号格式有误',
        icon: 'none'
      })
      return
    }
    var that = this
    let adres = that.data.region[0] + that.data.region[1] + that.data.region[2]
    wx.showLoading({
      title: '提交中',
      mask: true
    })
    api.post(formsubmit + '?name=Form', {
      Name: e.detail.value.Name,
      Phone: e.detail.value.Phone,
      Form: [{
        Key: 12,
        Value: e.detail.value.occupation
      }, 
      {
        Key: 13,
        Value: adres
      },
       {
        Key: 15,
        Value: that.data.array[that.data.index]
      }, 
      {
        Key: 14,
        Value: e.detail.value.villageName
      },
      {
        Key: 16,
        Value: e.detail.value.villageNum
      }, 
      {
        Key: 17,
        Value: e.detail.value.textarea
      }
      ]
    }).then(res => {
      wx.hideLoading()
      wx.showToast({
        title: '提交成功',
      })
      setTimeout(function () {
        wx.switchTab({
          url: '/pages/my/my',
        })
      }, 1500)
    }).catch(err => {

    })
  }
})