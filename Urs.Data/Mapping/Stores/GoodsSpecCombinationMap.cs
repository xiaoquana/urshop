
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Urs.Data.Domain.Stores;

namespace Urs.Data.Mapping.Stores
{
    public partial class GoodsSpecCombinationMap : UrsEntityTypeConfiguration<GoodsSpecCombination>
    {
        public override void Configure(EntityTypeBuilder<GoodsSpecCombination> builder)
        {
            builder.ToTable(nameof(GoodsSpecCombination));
            builder.HasKey(pvac => pvac.Id);
            builder.Property(pvac => pvac.AttributesXml);
            builder.Property(pv => pv.Sku).HasMaxLength(400);

            builder.HasOne(pvac => pvac.Goods)
                .WithMany(pv => pv.GoodsSpecCombinations)
                .HasForeignKey(pvac => pvac.GoodsId);
            base.Configure(builder);
        }
    }
}