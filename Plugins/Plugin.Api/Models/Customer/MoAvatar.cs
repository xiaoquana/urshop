﻿namespace Plugin.Api.Models.User
{
    /// <summary>
    /// 用户头像
    /// </summary>
    public partial class MoAvatar
    {
        /// <summary>
        /// 头像Id
        /// </summary>
        public int AvatarPictureId { get; set; }
        /// <summary>
        /// 用户识别（Guid)
        /// </summary>
        public string Guid { get; set; }
        /// <summary>
        /// 头像url
        /// </summary>
        public string AvatarUrl { get; set; }
    }
}