namespace Urs.Core.Plugins
{
    public enum LoadPluginsMode
    {
        All = 0,

        InstalledOnly = 10,

        NotInstalledOnly = 20
    }
}