﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using Urs.Admin.Models.Common;
using Urs.Data.Domain.Stores;
using Urs.Framework;
using Urs.Framework.Kendoui;
using Urs.Framework.Models;
using Urs.Framework.Mvc;

namespace Urs.Admin.Models.Orders
{
    public partial class OrderModel : BaseEntityModel
    {
        public OrderModel()
        {
            Items = new List<OrderGoodsModel>();
            Warnings = new List<string>();
        }
        [UrsDisplayName("Admin.Orders.Fields.Remark")]
        public string Remark { get; set; }
        [UrsDisplayName("Admin.Orders.Fields.CouponId")]
        public int? CouponId { get; set; }

        [UrsDisplayName("Admin.Orders.Fields.CouponMsg")]
        public string CouponMsg { get; set; }
        //identifiers
        [UrsDisplayName("Admin.Orders.Fields.ID")]
        public override int Id { get; set; }
        [UrsDisplayName("Admin.Orders.Fields.OrderGuid")]
        public Guid OrderGuid { get; set; }

        //user info
        [UrsDisplayName("Admin.Orders.Fields.User")]
        public int UserId { get; set; }

        [UrsDisplayName("Admin.Orders.Fields.Phone")]
        public string Phone { get; set; }
        [UrsDisplayName("Admin.Orders.Fields.ShippingContacts")]
        public string ShippingContacts { get; set; }
        public string UserFullName { get; set; }
        [UrsDisplayName("Admin.Orders.Fields.UserIP")]
        public string UserIp { get; set; }

        [UrsDisplayName("Admin.Orders.Fields.Affiliate")]
        public int? AffiliateId { get; set; }

        //totals
        [UrsDisplayName("Admin.Orders.Fields.OrderSubtotalExclTax")]
        public string OrderSubtotal { get; set; }
        [UrsDisplayName("Admin.Orders.Fields.OrderShippingExclTax")]
        public string OrderShipping { get; set; }
        [UrsDisplayName("Admin.Orders.Fields.PaymentMethodAdditionalFeeExclTax")]
        public string PaymentMethodAdditionalFee { get; set; }
        [UrsDisplayName("Admin.Orders.Fields.RedeemedRewardPoints")]
        public int RedeemedRewardPoints { get; set; }
        [UrsDisplayName("Admin.Orders.Fields.RedeemedRewardPoints")]
        public string RedeemedRewardPointsAmount { get; set; }
        [UrsDisplayName("Admin.Orders.Fields.OrderTotal")]
        public string OrderTotal { get; set; }
        [UrsDisplayName("Admin.Orders.Fields.RefundedAmount")]
        public string RefundedAmount { get; set; }
        [UrsDisplayName("Admin.Orders.Fields.AccountAmount")]
        public string AccountAmount { get; set; }
        //edit totals
        [UrsDisplayName("Admin.Orders.Fields.Edit.OrderSubtotal")]
        public decimal OrderSubtotalValue { get; set; }
        [UrsDisplayName("Admin.Orders.Fields.Edit.OrderShipping")]
        public decimal OrderShippingValue { get; set; }
        [UrsDisplayName("Admin.Orders.Fields.Edit.PaymentMethodAdditionalFee")]
        public decimal PaymentMethodAdditionalFeeValue { get; set; }
        [UrsDisplayName("Admin.Orders.Fields.Edit.OrderTotal")]
        public decimal OrderTotalValue { get; set; }

        //associated recurring payment id
        [UrsDisplayName("Admin.Orders.Fields.RecurringPayment")]
        public int RecurringPaymentId { get; set; }

        //order status
        [UrsDisplayName("Admin.Orders.Fields.OrderStatus")]
        public string OrderStatus { get; set; }
        [UrsDisplayName("Admin.Orders.Fields.OrderStatus")]
        public int OrderStatusId { get; set; }
        //payment info
        [UrsDisplayName("Admin.Orders.Fields.PaymentStatus")]
        public string PaymentStatus { get; set; }
        [UrsDisplayName("Admin.Orders.Fields.PaymentStatus")]
        public int PaymentStatusId { get; set; }
        [UrsDisplayName("Admin.Orders.Fields.PaymentMethod")]
        public string PaymentMethod { get; set; }
        //shipping info
        public bool IsShippable { get; set; }
        [UrsDisplayName("Admin.Orders.Fields.ShippingStatus")]
        public string ShippingStatus { get; set; }
        [UrsDisplayName("Admin.Orders.Fields.ShippingStatus")]
        public int ShippingStatusId { get; set; }
        [UrsDisplayName("Admin.Orders.Fields.ShippingAddress")]
        public AddressModel ShippingAddress { get; set; }
        [UrsDisplayName("Admin.Orders.Fields.ShippingMethod")]
        public string ShippingMethod { get; set; }
        public IList<OrderGoodsModel> Items { get; set; }
        //creation date
        [UrsDisplayName("Admin.Orders.Fields.CreateTime")]
        public DateTime CreateTime { get; set; }

        //checkout attributes
        public string CheckoutAttributeInfo { get; set; }

        public bool DisplayPdfInvoice { get; set; }


        //refund info
        [UrsDisplayName("Admin.Orders.Fields.PartialRefund.AmountToRefund")]
        public decimal AmountToRefund { get; set; }
        public decimal MaxAmountToRefund { get; set; }
        public string PrimaryStoreCurrencyCode { get; set; }

        //workflow info
        public bool CanCancelOrder { get; set; }
        public bool CanCapture { get; set; }
        public bool CanMarkOrderAsPaid { get; set; }
        public bool CanRefund { get; set; }
        public bool CanRefundOffline { get; set; }
        public bool CanPartiallyRefund { get; set; }
        public bool CanPartiallyRefundOffline { get; set; }
        public bool CanVoid { get; set; }
        public bool CanVoidOffline { get; set; }
        //warnings
        public List<string> Warnings { get; set; }

        #region Nested Classes

        public partial class OrderGoodsModel : BaseEntityModel
        {
            public OrderGoodsModel()
            {
                AfterSalesIds = new List<int>();
            }
            public int GoodsId { get; set; }
            public string GoodsName { get; set; }
            public string PictureThumbnailUrl { get; set; }
            public string Sku { get; set; }

            public string UnitPrice { get; set; }
            public decimal UnitPriceValue { get; set; }

            public int Quantity { get; set; }

            public string SubTotal { get; set; }
            public decimal SubTotalValue { get; set; }

            public string AttributeInfo { get; set; }
            public string RecurringInfo { get; set; }
            public IList<int> AfterSalesIds { get; set; }

        }

        public partial class OrderNote : BaseEntityModel
        {
            public int OrderId { get; set; }
            [UrsDisplayName("Admin.Orders.OrderNotes.Fields.DisplayToUser")]
            public bool DisplayToUser { get; set; }
            [UrsDisplayName("Admin.Orders.OrderNotes.Fields.Note")]
            public string Note { get; set; }
            [UrsDisplayName("Admin.Orders.OrderNotes.Fields.CreateTime")]
            public DateTime CreateTime { get; set; }
        }

        public partial class UploadLicenseModel : BaseModel
        {
            public int OrderId { get; set; }

            public int OrderGoodsId { get; set; }

        }

        public partial class AddOrderGoodsModel : BaseModel
        {
            public AddOrderGoodsModel()
            {
                AvailableCategories = new List<SelectListItem>();
                AvailableBrands = new List<SelectListItem>();
            }

            [UrsDisplayName("Admin.Store.Goods.List.SearchGoodsName")]

            public string SearchGoodsName { get; set; }
            [UrsDisplayName("Admin.Store.Goods.List.SearchCategory")]
            public int SearchCategoryId { get; set; }
            [UrsDisplayName("Admin.Store.Goods.List.SearchBrand")]
            public int SearchBrandId { get; set; }

            public IList<SelectListItem> AvailableCategories { get; set; }
            public IList<SelectListItem> AvailableBrands { get; set; }

            public int OrderId { get; set; }

            #region Nested classes

            public partial class GoodsLineModel : BaseEntityModel
            {
                [UrsDisplayName("Admin.Orders.Goodss.AddNew.Name")]

                public string Name { get; set; }

                [UrsDisplayName("Admin.Orders.Goodss.AddNew.SKU")]

                public string Sku { get; set; }
            }

            public partial class GoodsDetailsModel : BaseModel
            {
                public GoodsDetailsModel()
                {
                    GoodsSpecs = new List<GoodsVariantAttributeModel>();
                    Warnings = new List<string>();
                }

                public int GoodsId { get; set; }

                public int OrderId { get; set; }

                public string Name { get; set; }

                [UrsDisplayName("Admin.Orders.Goodss.AddNew.UnitPrice")]
                public decimal UnitPrice { get; set; }

                [UrsDisplayName("Admin.Orders.Goodss.AddNew.Quantity")]
                public int Quantity { get; set; }

                [UrsDisplayName("Admin.Orders.Goodss.AddNew.SubTotal")]
                public decimal SubTotal { get; set; }

                //goods attrbiutes
                public IList<GoodsVariantAttributeModel> GoodsSpecs { get; set; }

                public List<string> Warnings { get; set; }

            }

            public partial class GoodsVariantAttributeModel : BaseEntityModel
            {
                public GoodsVariantAttributeModel()
                {
                    Values = new List<GoodsSpecValueModel>();
                }

                public int GoodsSpecId { get; set; }

                public string Name { get; set; }

                public string TextPrompt { get; set; }

                public bool IsRequired { get; set; }

                public AttributeControlType AttributeControlType { get; set; }

                public IList<GoodsSpecValueModel> Values { get; set; }
            }

            public partial class GoodsSpecValueModel : BaseEntityModel
            {
                public string Name { get; set; }

                public bool IsPreSelected { get; set; }
            }
            #endregion
        }

        #endregion
    }
}