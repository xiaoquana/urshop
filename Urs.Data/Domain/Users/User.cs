using System;
using System.Collections.Generic;
using System.Linq;
using Urs.Core;
using Urs.Data.Domain.Common;
using Urs.Data.Domain.Orders;

namespace Urs.Data.Domain.Users
{
    /// <summary>
    /// Represents a user
    /// </summary>
    public partial class User : BaseEntity
    {
        private ICollection<ExternalAuth> _externalAuthRecords;
        private ICollection<UserRoleMapping> _userRoleMappings;
        private ICollection<UserRole> _userRoles;
        private ICollection<ShoppingCartItem> _shoppingCartItems;
        private ICollection<PointsHistory> _pointsHistory;
        protected ICollection<UserAddressMapping> _userAddressMappings;

        public User()
        {
            this.UserGuid = Guid.NewGuid();
            this.PasswordFormat = PasswordFormat.Clear;
        }
        /// <summary>
        /// 用户Guid
        /// </summary>
        public virtual Guid UserGuid { get; set; }
        public virtual string Nickname { get; set; }
        /// <summary>
        /// 真实姓名
        /// </summary>
        public virtual string Realname { get; set; }
        /// <summary>
        /// 公司
        /// </summary>
        public virtual string Company { get; set; }
        /// <summary>
        /// 用户码，用于邀请
        /// </summary>
        public virtual string Code { get; set; }
        public virtual string Email { get; set; }
        public virtual string PhoneNumber { get; set; }
        public virtual string Password { get; set; }
        public virtual int PasswordFormatId { get; set; }
        public virtual PasswordFormat PasswordFormat
        {
            get { return (PasswordFormat)PasswordFormatId; }
            set { this.PasswordFormatId = (int)value; }
        }
        public virtual string PasswordSalt { get; set; }
        public int AvatarPictureId { get; set; }
        /// <summary>
        /// Gets or sets the admin comment
        /// </summary>
        public virtual string AdminComment { get; set; }
        /// <summary>
        /// Gets or sets the affiliate identifier
        /// </summary>
        public virtual int? AffiliateId { get; set; }
        /// <summary>
        /// 是否有商品
        /// </summary>
        public bool HasShoppingCartItems { get; set; }
        /// <summary>
        /// 是否通过审核
        /// </summary>
        public virtual bool Approved { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether the user is active
        /// </summary>
        public virtual bool Active { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether the user has been deleted
        /// </summary>
        public virtual bool Deleted { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether the user account is system
        /// </summary>
        public virtual bool IsSystemAccount { get; set; }
        /// <summary>
        /// Gets or sets the user system name
        /// </summary>
        public virtual string SystemName { get; set; }
        /// <summary>
        /// Gets or sets the last IP address
        /// </summary>
        public virtual string LastIpAddress { get; set; }
        /// <summary>
        /// Gets or sets the date and time of entity creation
        /// </summary>
        public virtual DateTime CreateTime { get; set; }
        public virtual DateTime? LastLoginTime { get; set; }
        public virtual DateTime LastActivityTime { get; set; }
        public virtual int ShopId { get; set; }

        #region Navigation properties
        public virtual ICollection<ExternalAuth> ExternalAuthRecords
        {
            get { return _externalAuthRecords ?? (_externalAuthRecords = new List<ExternalAuth>()); }
            protected set { _externalAuthRecords = value; }
        }

        public virtual ICollection<UserRole> UserRoles
        {
            get => _userRoles ?? (_userRoles = UserRoleMappings.Select(mapping => mapping.UserRole).ToList());
        }

        public virtual ICollection<UserRoleMapping> UserRoleMappings
        {
            get => _userRoleMappings ?? (_userRoleMappings = new List<UserRoleMapping>());
            protected set => _userRoleMappings = value;
        }
        /// <summary>
        /// Gets or sets shopping cart items
        /// </summary>
        public virtual ICollection<ShoppingCartItem> ShoppingCartItems
        {
            get { return _shoppingCartItems ?? (_shoppingCartItems = new List<ShoppingCartItem>()); }
            protected set { _shoppingCartItems = value; }
        }

        public virtual ICollection<PointsHistory> PointsHistory
        {
            get { return _pointsHistory ?? (_pointsHistory = new List<PointsHistory>()); }
            protected set { _pointsHistory = value; }
        }
        
        public int? ShippingAddressId { get; set; }

        /// <summary>
        /// Default shipping address
        /// </summary>
        public virtual Address ShippingAddress { get; set; }


        public IList<Address> Addresses => UserAddressMappings.Select(mapping => mapping.Address).ToList();

        public virtual ICollection<UserAddressMapping> UserAddressMappings
        {
            get => _userAddressMappings ?? (_userAddressMappings = new List<UserAddressMapping>());
            protected set => _userAddressMappings = value;
        }
        
        #endregion
    }
}