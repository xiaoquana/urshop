﻿namespace Plugin.Api.Models.User
{
    /// <summary>
    /// 用户信息
    /// </summary>
    public partial class MoSimpleUser
    {
        /// <summary>
        /// 用户标识Guid
        /// </summary>
        public string Guid { get; set; }
        /// <summary>
        /// 电话
        /// </summary>
        public string Phone { get; set; }
    }
}