using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Globalization;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using Urs.Core.ComponentModel;
using Urs.Core.Infrastructure;
using Urs.Data.Domain.Shipping;

namespace Urs.Core
{
    /// <summary>
    /// Represents a common helper
    /// </summary>
    public partial class CustomHelper
    {
        public static TypeConverter GetCustomTypeConverter(Type type)
        {
            if (type == typeof(List<int>))
                return new GenericListTypeConverter<int>();
            if (type == typeof(List<decimal>))
                return new GenericListTypeConverter<decimal>();
            if (type == typeof(List<string>))
                return new GenericListTypeConverter<string>();
            if(type==typeof(Dictionary<string,string>))
                return new GenericDictionaryTypeConverter();
            if (type == typeof(ShippingOption))
                return new ShippingOptionTypeConverter();
            if (type == typeof(List<ShippingOption>) || type == typeof(IList<ShippingOption>))
                return new ShippingOptionListTypeConverter();

            return TypeDescriptor.GetConverter(type);
        }
    }
}
