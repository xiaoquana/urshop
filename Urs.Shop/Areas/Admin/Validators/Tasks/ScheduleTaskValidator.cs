﻿using FluentValidation;
using Urs.Admin.Models.Directory;
using Urs.Admin.Models.Tasks;
using Urs.Services.Localization;
using Urs.Framework.Validators;

namespace Urs.Admin.Validators.Tasks
{
    public class ScheduleTaskValidator : BaseUrsValidator<ScheduleTaskModel>
    {
        public ScheduleTaskValidator(ILocalizationService localizationService)
        {
            RuleFor(x => x.Name).NotNull().WithMessage(localizationService.GetResource("Admin.System.ScheduleTasks.Name.Required"));
            RuleFor(x => x.Seconds).GreaterThan(0).WithMessage(localizationService.GetResource("Admin.System.ScheduleTasks.Seconds.Positive"));
        }
    }
}