﻿using System.Collections.Generic;
using Urs.Core.Configuration;

namespace Urs.Data.Domain.Configuration
{
    public class OrderSettings : ISettings
    {
        /// <summary>
        /// Gets or sets a value indicating whether user can make re-order
        /// </summary>
        public bool IsReOrderAllowed { get; set; }
        /// <summary>
        /// Gets or sets a minimum order subtotal amount
        /// </summary>
        public decimal MinOrderSubtotalAmount { get; set; }

        /// <summary>
        /// Gets or sets a minimum order total amount
        /// </summary>
        public decimal MinOrderTotalAmount { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether anonymous checkout allowed
        /// </summary>
        public bool AnonymousCheckoutAllowed { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether 'One-page checkout' is enabled
        /// </summary>
        public int TimeOffUnpaidOrderMinutes { get; set; }
        public int TimeOffUnpaidOrderRange { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether "Return requests" are allowed
        /// </summary>
        public bool AfterSalesEnabled { get; set; }
        /// <summary>
        /// Gets or sets a list of return request reasons
        /// </summary>
        public List<string> AfterSalesReasons { get; set; }
        /// <summary>
        /// Gets or sets a list of return request actions
        /// </summary>
        public List<string> AfterSalesActions { get; set; }
        public bool CancelEnabled { get; set; }
        public bool DeleteEnabled { get; set; }
        /// <summary>
        /// Gets or sets a number of days that the Return Request Link will be available for users after order placing.
        /// </summary>
        public int NumberOfDaysAfterSalesAvailable { get; set; }
        /// <summary>
        /// Gets or sets an order placement interval in seconds (prevent 2 orders being placed within an X seconds time frame).
        /// </summary>
        public int MinimumOrderPlacementInterval { get; set; }
        public string OrderProcessingWarn { get; set; }

        public string LiJiaAppKey { get; set; }
        public string LiJiaSecretKey { get; set; }//密钥

    }
}