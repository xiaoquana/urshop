﻿namespace Urs.Core
{
    public class TokenEntity
    {
        public string token { get; set; }
        public string guid { get; set; }
        public string openId { get; set; }

        public string state { get; set; }
    }
}
