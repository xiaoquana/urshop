﻿using Microsoft.AspNetCore.Routing;

namespace Urs.Framework.Mvc.Routes
{
    public interface IRouteProvider{
        void RegisterRoutes(IRouteBuilder routes);

        int Priority { get; }
    }
}
