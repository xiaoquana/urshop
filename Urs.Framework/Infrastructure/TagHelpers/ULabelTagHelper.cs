﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace Urs.Framework.Infrastructure.TagHelpers
{
    [HtmlTargetElement("u-label", Attributes = "asp-for", TagStructure = TagStructure.WithoutEndTag)]
    public class ULabelTagHelper : TagHelper
    {
        public IHtmlGenerator Generator { get; set; }

        [HtmlAttributeName("asp-for")]
        public ModelExpression For { get; set; }

        [HtmlAttributeNotBound]
        [ViewContext]
        public ViewContext ViewContext { get; set; }

        public ULabelTagHelper(IHtmlGenerator generator)
        {
            Generator = generator;
        }
        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            output.SuppressOutput();
            var htmlContext = Generator.GenerateLabel(ViewContext, For.ModelExplorer, For.Name, null, new { @class = "layui-form-label" });
            output.Content.SetHtmlContent(htmlContext);
        }
    }
}
