using System;
using System.Collections.Generic;
using Urs.Core;
using Urs.Data.Domain.Common;
using Urs.Data.Domain.Payments;
using Urs.Data.Domain.Shipping;
using Urs.Data.Domain.Users;

namespace Urs.Data.Domain.Orders
{
    /// <summary>
    /// Represents an order
    /// </summary>
    public partial class Order : BaseEntity
    {
        private ICollection<OrderNote> _orderNotes;
        private ICollection<OrderItem> _orderItems;
        private ICollection<Shipment> _shipments;

        #region Properties
        public virtual string Code { get; set; }
        public virtual Guid OrderGuid { get; set; }
        public virtual int UserId { get; set; }
        public virtual int? CouponId { get; set; }
        public virtual int? ShippingAddressId { get; set; }
        public virtual int OrderStatusId { get; set; }
        public virtual int ShippingStatusId { get; set; }
        public virtual int PaymentStatusId { get; set; }
        public virtual string PaymentMethodSystemName { get; set; }
        public virtual decimal OrderSubtotal { get; set; }
        public virtual decimal OrderShipping { get; set; }
        public virtual decimal PaymentMethodAdditionalFee { get; set; }
        public virtual string PaymentBankName { get; set; }
        public virtual decimal ExchangePointsAmount { get; set; }
        public virtual decimal AccountAmount { get; set; }
        public virtual decimal OrderTotal { get; set; }
        public virtual string PayTransactionId { get; set; }
        public virtual string PayInitalRequest { get; set; }
        public virtual string PayInitalResponse { get; set; }
        public virtual decimal RefundedAmount { get; set; }
        public virtual bool RewardPointsWereAdded { get; set; }
        public virtual int? RewardPointsHistoryEntryId { get; set; }
        public virtual string CheckoutAttributeDescription { get; set; }
        public virtual string CheckoutAttributesXml { get; set; }
        public virtual string Remark { get; set; }
        public virtual int? AffiliateId { get; set; }
        public virtual string OrderIp { get; set; }
        public virtual DateTime? PaidTime { get; set; }
        public virtual string ShippingMethod { get; set; }
        public virtual string ShippingRateMethodSystemName { get; set; }
        public virtual bool Deleted { get; set; }
        public virtual DateTime CreateTime { get; set; }
        public virtual int ShopId { get; set; }
        #endregion

        #region Navigation properties

        /// <summary>
        /// Gets or sets the user
        /// </summary>
        public virtual User User { get; set; }

        /// <summary>
        /// Gets or sets the shipping address
        /// </summary>
        public virtual Address ShippingAddress { get; set; }

        /// <summary>
        /// Gets or sets the reward points history record
        /// </summary>
        public virtual PointsHistory PointsEntry { get; set; }

        /// <summary>
        /// Gets or sets order notes
        /// </summary>
        public virtual ICollection<OrderNote> OrderNotes
        {
            get { return _orderNotes ?? (_orderNotes = new List<OrderNote>()); }
            protected set { _orderNotes = value; }
        }

        /// <summary>
        /// Gets or sets order goods s
        /// </summary>
        public virtual ICollection<OrderItem> orderItems
        {
            get { return _orderItems ?? (_orderItems = new List<OrderItem>()); }
            protected set { _orderItems = value; }
        }

        /// <summary>
        /// Gets or sets shipments
        /// </summary>
        public virtual ICollection<Shipment> Shipments
        {
            get { return _shipments ?? (_shipments = new List<Shipment>()); }
            protected set { _shipments = value; }
        }

        #endregion

        #region Custom properties

        /// <summary>
        /// Gets or sets the order status
        /// </summary>
        public virtual OrderStatus OrderStatus
        {
            get
            {
                return (OrderStatus)this.OrderStatusId;
            }
            set
            {
                this.OrderStatusId = (int)value;
            }
        }

        /// <summary>
        /// Gets or sets the payment status
        /// </summary>
        public virtual PaymentStatus PaymentStatus
        {
            get
            {
                return (PaymentStatus)this.PaymentStatusId;
            }
            set
            {
                this.PaymentStatusId = (int)value;
            }
        }

        /// <summary>
        /// Gets or sets the shipping status
        /// </summary>
        public virtual ShippingStatus ShippingStatus
        {
            get
            {
                return (ShippingStatus)this.ShippingStatusId;
            }
            set
            {
                this.ShippingStatusId = (int)value;
            }
        }

        #endregion
    }
}
