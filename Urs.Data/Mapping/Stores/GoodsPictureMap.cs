
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Urs.Data.Domain.Stores;

namespace Urs.Data.Mapping.Stores
{
    public partial class GoodsPictureMap : UrsEntityTypeConfiguration<GoodsPicture>
    {
        public override void Configure(EntityTypeBuilder<GoodsPicture> builder)
        {
            builder.ToTable(nameof(GoodsPicture));
            builder.HasKey(pp => pp.Id);
            
            base.Configure(builder);
        }
    }
}