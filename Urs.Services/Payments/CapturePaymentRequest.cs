using Urs.Data.Domain.Orders;

namespace Urs.Services.Payments
{
    public partial class CapturePaymentRequest
    {
        public Order Order { get; set; }
    }
}
