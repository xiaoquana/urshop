using Urs.Data.Domain.Users;
using Urs.Data.Domain.Orders;

namespace Urs.Services.Payments
{
    public partial class PostProcessPaymentRequest
    {
        public Order Order { get; set; }
        public User User { get; set; }
        public string PaymentMethodSystemName { get; set; }
    }
}
