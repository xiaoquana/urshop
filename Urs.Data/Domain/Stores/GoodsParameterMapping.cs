using Urs.Core;
namespace Urs.Data.Domain.Stores
{
    /// <summary>
    /// 商品参数
    /// </summary>
    public partial class GoodsParameterMapping : BaseEntity
    {
        /// <summary>
        /// 商品Id
        /// </summary>
        public virtual int GoodsId { get; set; }

        public virtual int GoodsParameterOptionId { get; set; }

        /// <summary>
        /// 自定义值
        /// </summary>
        public virtual string CustomValue { get; set; }

        public virtual bool AllowFiltering { get; set; }

        public virtual int DisplayOrder { get; set; }
        
        public virtual GoodsParameterOption GoodsParameterOption { get; set; }
    }
}
