﻿
using System.IO;
namespace Urs.Services.ExportImport
{
    public partial interface IImportManager
    {
        void ImportGoodssFromXlsx(Stream stream);

        void ImportBrandsFromXlsx(Stream stream);
    }
}
