import api from '../../api/api'
import { orderDetail } from '../../api/conf'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    info:null,
    orderDetail: null,
    payShow: true,
    weixinName: null
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let infoObj = JSON.parse(options.info)
    this.setData({
      info: infoObj,
      payShow: infoObj.payShow
    })
    if (!infoObj.payShow){
      return
    }
    this.getOrderDetail()
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },
  ckdd:function(){
    wx.navigateTo({
      url: '/pages/orderList/orderList?status=20',
    })
  },
  copy:function(){
    let str = this.data.info.orderId.toString()
    wx.setClipboardData({
      data: str,
      success: function(res){
        wx.showToast({
          title: '复制成功',
        })
      }
    })
  },
  getOrderDetail:function(){
    wx.showLoading({
      title: '加载中',
    })
    api.get(orderDetail + '?oid=' + this.data.info.orderId,{
    }).then((res)=>{
      this.setData({
        orderDetail: res.Data
      })
      wx.hideLoading()
    })
  },
  onShareAppMessage:function(res){
    let obj = {
      payShow: false,
      orderId: this.data.orderDetail.Id,
      time: this.data.orderDetail.CreatedOn,
      items: this.data.orderDetail.Items,
      weixinName: wx.getStorageSync('nickName'),
      address: this.data.orderDetail.ShippingAddress,
      price: this.data.orderDetail.OrderTotal
    }
    let share = JSON.stringify(obj)
    if (res.from === 'button'){
      return {
        title: '通知团长',
        path: '/pages/payFinish/payFinish?info=' + share
      }
    }
  },
  goHome:function(){
    wx.switchTab({
      url: '/pages/index/index',
    })
  }
})