﻿using Urs.Core;

using System;

namespace Urs.Data.Domain.Tasks
{
    public class ScheduleTask : BaseEntity
    {
        /// <summary>
        /// Gets or sets the name
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Gets or sets the run period (in seconds)
        /// </summary>
        public virtual int Seconds { get; set; }

        /// <summary>
        /// Gets or sets the type of appropriate ITask class
        /// </summary>
        public virtual string Type { get; set; }

        public virtual bool Enabled { get; set; }

        public virtual bool StopOnError { get; set; }

        public virtual DateTime? LastStartTime { get; set; }

        public virtual DateTime? LastEndTime { get; set; }

        public virtual DateTime? LastSuccessTime { get; set; }
    }
}
