﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Urs.Data.Domain.Coupons;

namespace Urs.Data.Mapping.Coupons
{
    public partial class CouponMap : UrsEntityTypeConfiguration<Coupon>
    {
        public override void Configure(EntityTypeBuilder<Coupon> builder)
        {
            builder.ToTable(nameof(Coupon));
            builder.HasKey(p => p.Id);

            builder.Property(p => p.MinimumConsumption).HasColumnType("decimal(18, 2)");
            builder.Property(p => p.Value).HasColumnType("decimal(18, 2)");

            base.Configure(builder);

        }
    }
}
